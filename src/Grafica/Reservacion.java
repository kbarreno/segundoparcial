/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafica;

import Archivos.Ciudades;
import Archivos.Habitaciones;
import Archivos.Lectura;
import Ingreso.Clientes;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author liz
 */
public class Reservacion {
    private StackPane root = new StackPane();
    private VBox  pantalla = new VBox();
    private VBox pantalla2= new VBox();
    private HBox  presen = new HBox();
    private String reser = new String();
    private ComboBox cb = new ComboBox();
    private ChoiceBox cbn = new ChoiceBox();
    private Label lblpro;
    private Label lblPre= new Label("Escoja el tipo de habitacion que desea: ");
    private ArrayList<Habitaciones> lista = new ArrayList();
    private int n2;
    private int n;
    private int m2;
    private int m;
    private int d2;
    private int d;
    private double total;
    private double tarifa;
    public Reservacion(ArrayList<Habitaciones> lista, String reser) {
        
        this.lista=lista;
        this.reser= reser;
        System.out.println(lista);
        comboBoxH();
        organizar();
        
    }

    public StackPane getRoot() {
        return root;
    }

    public VBox getPantalla() {
        return pantalla;
    }
    /**
     * organiza los nodos en la pantalla
     */
    public void organizar (){
        Image im = new Image("Grafica/fotogh.png");
        ImageView imagen = new ImageView(im);
        imagen.setPreserveRatio(true);
        imagen.setSmooth(true);
        root.getChildren().add(imagen);
        lblpro = new Label(reser);
        //lblPre = new Label(nombre);
        
        
        pantalla.setMargin(lblpro, new Insets(80, 20, 10, 300));
        pantalla.setMargin(presen, new Insets(10, 20, 10, 250));
        pantalla.setMargin(pantalla2, new Insets(1, 20, 10, 50));
        lblpro.setFont(new Font("Harrington", 30));
        lblPre.setFont(new Font("Arial", 17));
        presen.getChildren().addAll(lblPre,cb);
        presen.setSpacing(10);
        
        
        pantalla.getChildren().addAll(lblpro,presen,pantalla2);
                root.getChildren().add(pantalla);
        
        
        
        
        
    }
    
    /**
     * depende del hotel seleccionado se llena el combo box de habitaciones
     */
    public void comboBoxH() {
        //presen.getChildren().removeAll();
        
        ArrayList<Habitaciones> listaH = new ArrayList();
        for(Habitaciones h : lista){
            listaH.add(h);
            
        }
        cb.getItems().addAll(listaH);
        cb.setOnAction(e->mostrarHabitacion((Habitaciones) cb.getValue()) );
        
    } 
    /**
     * depende a los dias mes y año seleccionado calcula el total que se debe pagar si se realiza la reservacion 
     * debe escoger una de las 3 tarifas, si no el programa asumira que la tarifa es 0
     * @param d2
     * @param d
     * @param m2
     * @param m
     * @param n2
     * @param n
     * @param cli 
     */
    public void calcular(int d2,int d,int m2,int m,int n2,int n,String cli){
        System.out.println(d2);
        System.out.println(d);
        int tAno = n2-n;
        int tMes = m2-m;
        int tDia  =d2-d;    
        if(tAno==0){
            if(tMes==0){
                System.out.println("diaaa");
                total= tDia*tarifa;
                if(tDia==0){    
                    System.out.println("d-a");
                    total = 0;
                    
                }
            }else{
                total=((tMes*31)+(tDia))*tarifa;
            }
        }else{
            total=(((tAno*12)*31)+(tMes*31)+(tDia))*tarifa;
        }
        
        
        pantalla2.getChildren().add(new Label("                         "+"Estimado/a "+cli+" El valor total a pagar : "+total));
    }
   /**
    * al momento de escoger una habitacionse envia esta habitacion a este metodo y aparecera
    * el tipo de tarifas que ofrece, llena los combo box de dias mes y año
    *
    * @param habitacion 
    */
    
    public void mostrarHabitacion(Habitaciones habitacion){
        pantalla2.getChildren().clear();
        Label tarifaS = new Label("Tarifa sencilla: "+String.valueOf(habitacion.getTarifaSencilla())+" \n");
        Label tarifaD = new Label("            Tarifa doble: "+String.valueOf(habitacion.getTarifaDoble())+" \n");
        Label tarifaT = new Label ("           Tarifa Triple: "+String.valueOf(habitacion.getTarifaTriple())+" \n");
        
        Label mensaje = new Label("Seleccione la tarifa de su preferencia: ");
        
        Button taS = new Button("Tarifa Sencilla");
        Button taD = new Button("Tarifa Doble");
        Button taT = new Button("Tarifa Triple");
        
        
        Button confirmar = new Button("Confirmar datos");
        
        
        HBox calendario= new HBox();
        HBox tarifas= new HBox();
        
        ComboBox dia = new ComboBox();
        ComboBox mes = new ComboBox();
        ComboBox ano = new ComboBox();
        
        ComboBox dia2 = new ComboBox();
        ComboBox mes2 = new ComboBox();
        ComboBox ano2 = new ComboBox();
        
        for(int i = 1;i<32;i++){
            dia.getItems().addAll(i);
            dia2.getItems().addAll(i);
        }
        
        for(int i = 1;i<13;i++){
            mes.getItems().addAll(i);
            mes2.getItems().addAll(i);
        }
        for(int i = 2020;i<2030;i++){
           ano.getItems().addAll(i);
           ano2.getItems().addAll(i);
        }
        
        Label c = new Label("Seleccione que dia desea empezar y que dia desea terminar la reservacion en este formato dia/mes/año");
        calendario.getChildren().addAll(new Label("dia: "),dia,new Label(" mes: "),mes,new Label(" año: "),ano,
                                        new Label("                "),new Label("dia: "),dia2,new Label(" mes: "),mes2,
                                        new Label(" año: "),ano2);
        
        HBox hB= new HBox();
        HBox hFinal= new HBox();
         
        pantalla2.setMargin(tarifas, new Insets(10, 20, 10, 50));
        
        pantalla2.setMargin(hB, new Insets(10, 20, 10, 90));
        pantalla2.setMargin(c, new Insets(10, 20, 10, 35));
        pantalla2.setMargin(confirmar, new Insets(10, 20, 10, 100));
        
        pantalla2.setMargin(calendario, new Insets(10, 20, 10, 35));
        pantalla2.setMargin(mensaje, new Insets(10, 20, 10, 90));
        
        
        tarifaS.setFont(new Font("Arial", 15));
        tarifaD.setFont(new Font("Arial", 15));
        tarifaT.setFont(new Font("Arial", 15));
        mensaje.setFont(new Font("Arial", 15));
        c.setFont(new Font("Arial",15));
        
        taS.setOnAction(e->{
            tarifa=  habitacion.getTarifaSencilla();
                System.out.println(tarifa);
        });
        
        taD.setOnAction(e->{
            tarifa=  habitacion.getTarifaDoble();
                System.out.println(tarifa);
            
        });
        
        taT.setOnAction(e->{
             tarifa=  habitacion.getTarifaTriple();
                System.out.println(tarifa);
            
        });
        
        
        hB.getChildren().addAll(mensaje,taS,taD,taT);
        hB.setSpacing(15);
        
        
        dia.setOnAction(e->{
           d = (int) dia.getValue();
            System.out.println(d);
            
        }
        );
        
        dia2.setOnAction(e->{
             d2 = (int) dia2.getValue();
             System.out.println(d2);
            
        }
        );
        
        mes.setOnAction(e->{
             m = (int) mes.getValue();
            
        }
        );
        
        mes2.setOnAction(e->{
            m2 = (int) mes2.getValue();
            
        }
        );
        
        ano.setOnAction(e->{
            n = (int) ano.getValue();
            
        }
        );
        
        ano2.setOnAction(e->{
            n2 = (int) ano2.getValue();
            
        }
        );
        ComboBox clien = new ComboBox();
        ArrayList<Clientes> clientesL = new ArrayList<>();
        clientesL = Interfaz.clientesL;
        clien.getItems().addAll(clientesL);
        String cli = String.valueOf(clien.getValue());
        confirmar.setOnAction(e->calcular(d2,d,m2,m,n2,n,cli));
        
        
        
        
        hFinal.setSpacing(15);
        hFinal.getChildren().addAll(clien,confirmar);
        tarifas.getChildren().addAll(tarifaS,tarifaD, tarifaT);
        pantalla2.getChildren().addAll(tarifas,hB,c,calendario,hFinal);
        
        
        
        
    }
    
    
}
