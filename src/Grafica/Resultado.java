/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafica;

import Archivos.Catalogo;
import Archivos.Ciudades;
import Archivos.Habitaciones;
import Archivos.Hoteles;

import Archivos.HotelServicio;
import Archivos.Lectura;
import Archivos.Promociones;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author liz
 */
public class Resultado {
    private VBox  root = new VBox();
    private VBox  ventana2 = new VBox();
    private HBox  ventana = new HBox();
    private  ArrayList<HotelServicio> lista= new ArrayList<>();
    private ScrollPane spHotel = new ScrollPane();
    private ScrollPane sp = new ScrollPane();
    private ComboBox<Ciudades> cbHabitaciones = new ComboBox();
    private String m = new String();
    
    private Label lblpro = new Label("Provincias seleccionadas");
    /**
     * el constructor de esta clase pide una lista, ya que son los hoteles que son seleccionados por el usuario
     * @param lista 
     */
    public Resultado(ArrayList<HotelServicio> lista) {
        this.lista=lista;
        organizarControles();
        presentar();
    }
    /**
     * crea un string con el nombre del hotel y los espacion reemplazados para que el otro metodo solo tome el string
     * 
     * @param lineaMapa
     * @param idCiudad
     * @return 
     */
    public String Mapa(String lineaMapa,int idCiudad){
        String linea;
        linea = lineaMapa.replace(" ","%20");
        
        for(Ciudades c: Lectura.getIudades()){
            if(c.getIdCiudad()==idCiudad){
                String ciu= "+"+c.getCiudad();
                String replace = ciu.replace(" ","%20");
                m = replace;
                
            }
            
        }
        return linea+m;
        
    }
    
    
      /**
       * llena la pantalla de resultado con los hoteles que se envian de la anterior clase (buscador)
       * recorre la lista y se comparan con los hoteles guardados y si coinciden los ids
       * se presentan los hoteles con su foto e informacion
       */
    
    public void presentar(){
        for (HotelServicio h: lista){  
            for(Hoteles hotel: Lectura.getOteles()){
                if(h.getIdHotel()==hotel.getIdHotel()){
                    HBox  ventana = new HBox();
                    VBox  vHotel = new VBox();
                    ImageView im = new ImageView(hotel.getFoto());
                    String dres = hotel.getDescripcionHotel();
                    im.setFitWidth(150);//67676789879
                    im.setFitHeight(150);
                    Thread ti = new Thread(new CargadorImagen(im,hotel.getFoto()));
                    ti.start();
                    HBox imHotel = new HBox(im);
                    imHotel.setSpacing(10);
                    Label nombreHotel = new Label(hotel.getNombreHotel());
                    Label direccion = new Label("Direccion: "+hotel.getDireccion());
                    Button descripcion = new Button("Informacion");
                    Button reserva = new Button("Reservar en este hotel");
                    HBox  informacion = new HBox();
                    Button mapa = new Button("ver mapa");
                    Button registar = new Button("Registrar otro usuario");
                    //ventana.setMargin(i, new Insets(70, 40, 10, 10));
                    String info = "Tipos de habitaciones: \n ";
                    String reser = hotel.getNombreHotel();
                    String lineaMapa = h.getNombreHotel();
                    int idCiudad = hotel.getIdCiudad();
                    ArrayList<Habitaciones> lis = new ArrayList();
                    for(Habitaciones habitacion: Lectura.getAbitaciones()){
                        //lis.removeAll(lis);
                        if(h.getIdHotel()==habitacion.getIdHotel()){
                            
                            info = info+ habitacion.getTipoHabitacion()+"\nTarifa sencilla:  "+habitacion.getTarifaSencilla()+"  Tarifa doble:  "
                                    + habitacion.getTarifaDoble()+"  Tarifa triple:  "+ habitacion.getTarifaTriple()+"\n";
                            
                            lis.add(habitacion);
                        }    
                    }
                    info = info+ "Servicios: \n "+h.getServicio();
                    info = info +"\nDrescipcion del hotel: \n " +dres + "\n Pagina web del hotel: " +hotel.getWeb();
                    String fin = info;
                    ArrayList<Habitaciones> l = lis;
                    descripcion.setOnAction(event -> {
                                spHotel.setContent(ventana2);
                                Alert a =new Alert(Alert.AlertType.INFORMATION);
                                
                                a.setContentText(fin);
                                a.setTitle("j");
                                a.show();
                            });
                    reserva.setOnAction(event -> {
                                Stage st= (Stage)root.getScene().getWindow();
                                st.setTitle("Resultados");
                                st.getScene().setRoot(new Reservacion(l,reser).getRoot() );
                                st.centerOnScreen();
                            });
        
                    registar.setOnAction(event->{
                      Stage st= (Stage)root.getScene().getWindow();
                      st.setTitle("Resultados");
                      st.getScene().setRoot(new Interfaz().getRoot() );

                      st.centerOnScreen();

                    });
                    String ubicacion = Mapa(lineaMapa, idCiudad);
                                    mapa.setOnAction(event -> {
                                    URI myURI;
                                    try {
                                        String query= ubicacion;
                                        myURI = new URI("https://www.google.com/maps/search/?api=1&query="+query);
                                        Desktop.getDesktop().browse(myURI);


                                    } catch (URISyntaxException ex) {
                                        Logger.getLogger(Resultado.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (IOException ex) {
                                        Logger.getLogger(Resultado.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                            });
                    //mapa.setOnAction(e-> Mapa(h.getNombreHotel(),hotel.getIdCiudad()));
                    Mapa(lineaMapa,idCiudad);
                    informacion.getChildren().addAll(descripcion,mapa,reserva,registar);
                    informacion.setSpacing(17);
                    vHotel.getChildren().addAll(imHotel,new Label(""),nombreHotel,direccion,informacion);
                    vHotel.setSpacing(25);
                    ventana.getChildren().addAll(imHotel,vHotel);
                    ventana.setSpacing(15);
                    ventana2.getChildren().add(ventana);
                    ventana2.setSpacing(25);
                    
                    
                    
                }    
            }
            
              
              
          }
        spHotel.setContent(ventana2);
    }
    
    public void organizarControles(){
        root.setMargin(lblpro, new Insets(10, 20, 10, 350)); 
        lblpro.setFont(new Font("Cambria", 20));
        root.setMargin(spHotel, new Insets(10, 70, 10, 100)); 
        spHotel.setPrefSize(600, 700);
        root.getChildren().addAll(lblpro,spHotel);
        
    }
    
    public VBox getRoot() {
        return root;
    }
    
    class CargadorImagen implements Runnable{
        private ImageView img;
        private String urlIm;
        
        public CargadorImagen(ImageView img, String urlIm){
            this.urlIm = urlIm;
            this.img = img;
        }

        @Override
        public void run() {
            img.setImage(new Image(urlIm) );
            
        }
        
    }
}
