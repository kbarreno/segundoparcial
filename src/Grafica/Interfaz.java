/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafica;

import Archivos.Catalogo;
import Ingreso.Clientes;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
//import Grafica.BusquedaVergara;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.GroupBuilder;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import poosegundoparcial.PooSegundoParcial;
import javafx.stage.Stage;

/**
 *
 * @author liz
 */
public class Interfaz {
    
    private   StackPane root = new StackPane();
    private VBox circulo = new VBox();
    private Label lblnombre = new Label("Ingrese su nombre:");
    private TextField nombre = new TextField();
    private Label lblcedula = new Label("Ingrese su cedula:");
    private Label lb = new Label("Ingreso de datos");
    private TextField cedula = new TextField();
    private Label lbldireccion = new Label("Ingrese su direccion:");
    private TextField direccion = new TextField();
    private Label lblcorreo = new Label("Ingrese su correo electronico:");
    private TextField correo = new TextField();
    private Button guardar = new Button("Ingresar datos");
    private Label lblBusqueda = new Label("Busquedas de hoteles por:");
    private BorderPane raiz = new BorderPane();
    private HBox busquedas = new HBox();
    private Button buscador1 = new Button("Provincia y ciudad");//busqueda por ciudad y provincia
    private Button buscador2 = new Button("Hotel ó por servico");//buscador por hotel o servicio
    
    public static ArrayList<Clientes> clientesL = new ArrayList<>();

    public Label getLblnombre() {
        return lblnombre;
    }

    public TextField getNombre() {
        return nombre;
    }

    public Label getLblcedula() {
        return lblcedula;
    }

    public TextField getCedula() {
        return cedula;
    }

    public Label getLbldireccion() {
        return lbldireccion;
    }

    public TextField getDireccion() {
        return direccion;
    }

    public Label getLblcorreo() {
        return lblcorreo;
    }

    public TextField getCorreo() {
        return correo;
    }

    public Button getGuardar() {
        return guardar;
    }

    public ArrayList<Clientes> getClientesL() {
        return clientesL;
    }
    /**
     este metodo crea el cliente que el usuario ingresa y lo pone en una lista
     */
    public void ingresoCliente() {
        try{
          Clientes cli = new Clientes(Integer.parseInt(cedula.getText()), nombre.getText(),
          direccion.getText(), correo.getText());
          clientesL.add(cli);
          Stage st= (Stage)root.getScene().getWindow();
          st.setTitle("Buscador de hoteles ");
          st.getScene().setRoot(new Buscador().getRoot() );
          
          st.centerOnScreen();
        }
        catch(NumberFormatException e){
         Alert a =new Alert(Alert.AlertType.ERROR);
         a.setTitle("ERROR");
         a.show();
        }
        /**Clientes cli = new Clientes(Integer.parseInt(cedula.getText()), nombre.getText(),
        direccion.getText(), correo.getText());
        clientesL.add(cli);**/

    }

    public  StackPane getRoot() {
        return root;
    }

    public Label getLblsubtitulo1() {
        return lblnombre;
    }

    public Interfaz() {
        organizarControles();
    }
    
    public static void serializar(){
        try(ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("clientes.dat"))){
            o.writeObject(clientesL);
        }
        catch(IOException e){
            System.err.println(e);
        }
    }

    public static void deserializar(){
        try(ObjectInputStream i = new ObjectInputStream(new FileInputStream("clientes.dat"))){
           ArrayList<Clientes> clientesL = (ArrayList<Clientes>)i.readObject();
            System.out.println("Objeto Deserializado");
            System.out.println(clientesL);
        }
        catch(IOException e){
            System.err.println("Error en deserializar() "+e);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error en deserializar() "+ ex);
        }
    }
    /**
     Organiza todos los nodos de la pantalla crear clientes
     */
    public void organizarControles() {
        
       
        Image im = new Image("Grafica/fotop.png");
        ImageView imagen = new ImageView(im);
        imagen.setPreserveRatio(true);
        imagen.setSmooth(true);
        root.getChildren().add(imagen);
        root.setMargin(circulo, new Insets(200, 40, 10, 350));    
        circulo.setMargin(guardar, new Insets(0, 0, 0, 90));      
        
        nombre.setPromptText("Alguien");
        cedula.setPromptText("0924557818 (Sólo numeros)");
        direccion.setPromptText("Samanes mz. 305 villa 45");
        correo.setPromptText("Alguien@espol.edu.ec");
        
        busquedas.getChildren().addAll(buscador1,buscador2);
        
        buscador2.setAlignment(Pos.CENTER_RIGHT);
        lb.setFont(new Font("Harrington", 35));
        
        lblnombre.setFont(new Font("Arial", 14));
        
        lblcedula.setFont(new Font("Arial", 14));
        lbldireccion.setFont(new Font("Arial", 14));
        lblcorreo.setFont(new Font("Arial", 14));
        nombre.setAlignment(Pos.CENTER);//Align text to center
        nombre.setPrefWidth(20);
        nombre.setMinSize(300, 25);
        nombre.setMaxSize(300, 25);
        
        cedula.setMinSize(300, 25);
        cedula.setMaxSize(300, 25);
        
        
        direccion.setMinSize(300, 25);
        direccion.setMaxSize(300, 25);
        
        
        correo.setMinSize(300, 25);
        correo.setMaxSize(300, 25);
          //st.setHeight(600);
        lb.setStyle("-fx-font-weight: bold;");
        circulo.getChildren().addAll( lb,lblnombre, nombre, lblcedula, cedula, lbldireccion,
                direccion, lblcorreo, correo, guardar);
        circulo.setSpacing(7);//imagen
        guardar.setMinSize(100, 60);
        guardar.setMaxSize(100, 60);
        guardar.setWrapText(true);
        guardar.setAlignment(Pos.CENTER);
        lblBusqueda.setAlignment(Pos.TOP_CENTER);
        root.getChildren().add(circulo);
        
        
        //accion al boton
        guardar.setOnAction(e->ingresoCliente());
        
        
        
        
    }
    
    
    }

 