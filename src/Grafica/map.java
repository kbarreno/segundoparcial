/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafica;

import Archivos.HotelServicio;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author liz
 */
public class map {
    
    private StackPane root = new StackPane();
    private VBox  pantalla = new VBox();
    private HBox  ingreso = new HBox();
    private Button ver = new Button("ver resultados");
    private Label lb = new Label("Ingrese un sitio Turistico");
    private TextField buscar = new TextField(); 

    public StackPane getRoot() {
        return root;
    }

    public VBox getPantalla() {
        return pantalla;
    }

    public map() {
        organizarControles();
    }
    
    
    
     public void organizarControles(){
        
        //StackPane.setAlignment(hHoteles, Pos.CENTER); 
        Image im = new Image("Grafica/fotos.png");
        ImageView imagen = new ImageView(im);
        imagen.setPreserveRatio(true);
        imagen.setSmooth(true);
        root.getChildren().add(imagen);
        
        
        buscar.setPromptText("Ingrese un sitio turistico");
        lb.setFont(new Font("Harrington", 40));
        pantalla.setMargin(lb, new Insets(50, 10, 10, 80));
        pantalla.setMargin(ingreso, new Insets(100, 10, 10,190));
        ingreso.setSpacing(10);
        ingreso.getChildren().addAll(buscar,ver);
        pantalla.getChildren().addAll(ingreso,lb);
        root.getChildren().add(pantalla);
        //ver.setOnAction(e->verResultados(temporal));
        
     }  
     
    public void presentar(){
       String g = buscar.getText();
       
       //ver.setOnAction(e->verResultados(temporal));
       
    } 
    
    
     public void verResultados(ArrayList<HotelServicio> l){
        Stage st= (Stage)root.getScene().getWindow();
          st.setTitle("Resultados");
          st.getScene().setRoot(new Resultado(l).getRoot() );
          st.setWidth(900);
          st.setHeight(600);
          st.centerOnScreen();
          
          
        
    }
}
