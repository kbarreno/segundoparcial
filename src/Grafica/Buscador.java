/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafica;

import Archivos.Ciudades;
import Archivos.HotelServicio;
import Archivos.Hoteles;
import Archivos.Lectura;
import Archivos.Promociones;
import Archivos.Provincias;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author liz
 */
public class Buscador {
    private TableView tb = new TableView();
    private   StackPane root = new StackPane();
    private VBox  pantalla = new VBox();
    private TextField buscar = new TextField(); 
    private Label lblpro = new Label("Seleccione provincia");
    private HBox hHoteles = new HBox();
    private HBox hCiudades = new HBox();
    private HBox bus = new HBox();
    private Label lblciu = new Label("Ciudad:");
    private ComboBox<Provincias> cbProvincias = new ComboBox();
    private ComboBox<Ciudades> cbCiudades = new ComboBox();
    private ScrollPane spHotel = new ScrollPane();
    private ChoiceBox cb = new ChoiceBox();
    private Button ver = new Button("Ver Hoteles");
    private Button registar = new Button("Registrar otro usuario");
    private Button buscador = new Button("Búsqueda por proximidad a lugar turístico");

      

    public Buscador() {
        organizarControles();
        llenarComboBoxP();
        llenarHoteles(Lectura.getHoSe());
        Choice();
        
    }
    
    public StackPane getRoot() {
        return root;
    }
     /**
      * organiza todos los nodos de la pantalla buscador
      */
    public void organizarControles(){
        
        //StackPane.setAlignment(hHoteles, Pos.CENTER); 
        Image im = new Image("Grafica/fotos.png");
        ImageView imagen = new ImageView(im);
        imagen.setPreserveRatio(true);
        imagen.setSmooth(true);
        root.getChildren().add(imagen);
        
        pantalla.setMargin(hHoteles, new Insets(70, 20, 10, 300)); //(si es mayor se ira mas abajo,***,los separa entre si)
        pantalla.setMargin(hCiudades, new Insets(50, 20, 10, 380)); 
        pantalla.setMargin(spHotel, new Insets(100, 40, 10, 100)); 
        pantalla.setMargin(bus, new Insets(50, 20, 10, 350)); 
        pantalla.setMargin(buscador, new Insets(10, 40, 20, 388));
        pantalla.setMargin(registar, new Insets(10, 40, 20, 440));
        
        registar.setOnAction(event->{
          Stage st= (Stage)root.getScene().getWindow();
          st.setTitle("Resultados");
          st.getScene().setRoot(new Interfaz().getRoot() );
          
          st.centerOnScreen();
            
        });
        pantalla.getChildren().addAll(hHoteles,hCiudades,tb,bus,buscador,registar);
        bus.getChildren().addAll(cb,buscar,ver);
        
        bus.setSpacing(10);
        lblpro.setFont(new Font("Arial", 20));
        lblciu.setFont(new Font("Arial", 20)); 
        hHoteles.getChildren().addAll(lblpro,cbProvincias);
        hCiudades.getChildren().addAll(lblciu,cbCiudades);
        hHoteles.setSpacing(5);
        hCiudades.setSpacing(5);
        tb.setPrefSize(600, 700);
        
        spHotel.setContent(tb);
        cb.getItems().addAll("Servicio", "Hotel");
        buscar.setPromptText("Buscar...");
        root.getChildren().add(pantalla);
        buscador.setOnAction(e->busquedaProxima());
         
    }
     /**
      * 
      * llena el tableview con todos los hoteles 
      */
    public void llenarHoteles(ArrayList<HotelServicio> h){
        ObservableList<HotelServicio> data = FXCollections.observableArrayList(h);
         
        
        TableColumn nom = new TableColumn("Hotel");
        TableColumn servi = new TableColumn("Servicios");
        servi.setCellValueFactory(
               new PropertyValueFactory<HotelServicio, String>("servicio"));
        nom.setCellValueFactory(
                new PropertyValueFactory<HotelServicio, String>("nombreHotel"));
        
        tb.setItems(data);
        tb.getColumns().addAll(nom, servi);
        tb.setPrefSize(300, 350);
        
    }    
    /**
     * presenta la pantalla donde realiza la busqueda por nombre de lugar turistico
     */
    
    public void busquedaProxima(){
        
        Stage st= (Stage)root.getScene().getWindow();
          st.setTitle("Resultados");
          st.getScene().setRoot(new map().getRoot() );
          st.setWidth(600);
          st.setHeight(300);
          st.centerOnScreen();
        
    }
     /**
      * llenaelcombobox de las provincias
      */
    public void llenarComboBoxP(){
        
        tb.getItems().removeAll(); 
        cbCiudades.getItems().clear();
        cbProvincias.getItems().clear();
        ArrayList<Provincias> l = Lectura.getRovincias();
        cbProvincias.getItems().addAll(Lectura.getRovincias());
        cbProvincias.setOnAction(e -> comboBoxCiudades());
        
        }
     /**
      * crea un arraylist temporal con los hoteles que aparezcabn en el tableview(los que se hayan buscado)
      */
    public void mostrarHoteles(){
        tb.getItems().removeAll();
        ArrayList<Hoteles> hotel=new ArrayList();
        ArrayList<HotelServicio> temporal=new ArrayList();
        for (Hoteles h:Lectura.getOteles()){
            if(cbCiudades.getValue().getIdCiudad()==(h.getIdCiudad())){
                hotel.add(h);
            }
        }
       
        for (Hoteles ho: hotel){
            for(HotelServicio s: Lectura.getHoSe()){
                if(ho.getIdHotel()==s.getIdHotel()){
                    
                    temporal.add(s);
                }
            }
            
        }
        llenarHoteles(temporal);
        
        ver.setOnAction(e->verResultados(temporal));
        
        
    } 
    
    
    /**
     * llena el combobox de ciudades
     */
    public void comboBoxCiudades() {
        tb.getItems().removeAll();
        cbCiudades.getItems().clear();
        ArrayList<Ciudades> ciL = new ArrayList();
        int id =0;
        id = cbProvincias.getValue().getIdProvincia();
        for (Ciudades ci : Lectura.getIudades()) {
            if(ci.getIdProvincia()==id){
                ciL.add(ci);
            }
        }
        cbCiudades.getItems().addAll(ciL);
        cbCiudades.setOnAction(e->mostrarHoteles() );
       
    } 
    /**
     * filtrar los hoteles por nombre del hotel o por servicio, los filtra y los coloca en el table view
     */
    public void Choice(){
        ArrayList<HotelServicio> lista = Lectura.getHoSe();
        ObservableList<HotelServicio> data = FXCollections.observableArrayList(lista);
         
        String s = (String) cb.valueProperty().get();
        System.out.println(s);
        
        cb.setOnAction(event -> {
        if (cb.valueProperty().get().equals("Hotel")) {
            System.out.println("ghg");
            String b = buscar.getText();
            FilteredList<HotelServicio> filteredData = new FilteredList<>(data, p -> true);
            buscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(HotelServicio -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                    
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (String.valueOf(HotelServicio.getNombreHotel()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
                });
        });
        SortedList<HotelServicio> sortedData = new SortedList<>(filteredData);
  
        sortedData.comparatorProperty().bind(tb.comparatorProperty());
        
        tb.setItems(sortedData);
        
        ArrayList<HotelServicio> temporal=new ArrayList();
        for (int i=0; i < sortedData.size() ;i++){
            temporal.add(sortedData.get(i));
            System.out.println(sortedData.get(i));
            
            
        }
        ver.setOnAction(e->verResultados(temporal));
        }else if (cb.valueProperty().get().equals("Servicio")){
            
            FilteredList<HotelServicio> filteredData = new FilteredList<>(data, p -> true);
            buscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(HotelServicio -> {
                
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (String.valueOf(HotelServicio.getServicio()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                   

                }
                return false;
                });
        });
        SortedList<HotelServicio> sortedData = new SortedList<>(filteredData);

       
        sortedData.comparatorProperty().bind(tb.comparatorProperty());
        
        tb.setItems(sortedData);
        }    
        });
    
    
    }
    /**
     * 
     * envia los hoteles que aparezcan en el table view para que se presenten en resultados  
     */
    
    public void verResultados(ArrayList<HotelServicio> l){
        Stage st= (Stage)root.getScene().getWindow();
          st.setTitle("Resultados");
          st.getScene().setRoot(new Resultado(l).getRoot() );
          st.setWidth(900);
          st.setHeight(600);
          st.centerOnScreen();
          
          
        
    }
}