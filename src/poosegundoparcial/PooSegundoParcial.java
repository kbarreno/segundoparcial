/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poosegundoparcial;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import Grafica.Interfaz;
import Ingreso.Clientes;
import Archivos.Catalogo;
import Archivos.Ciudades;
import Archivos.Habitaciones;
import Archivos.Hoteles;
import Archivos.Lectura;
import Archivos.Provincias;
import Archivos.Servicios;
//import Grafica.BusquedaVergara;
import Grafica.Interfaz;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;



/**
 *
 * @author liz
 */
public class PooSegundoParcial extends Application{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Lectura.readFIles();
            Lectura.getAtalogos();
            Lectura.getErvicios();
            Lectura.getIudades();
            Lectura.getOteles();
            Lectura.getRovincias();
            //System.out.println("heeee"+Lectura.getHoSe());
             
            launch();
            //System.out.println();
        } catch (IOException ex) {
            Logger.getLogger(PooSegundoParcial.class.getName()).log(Level.SEVERE, null, ex);
        }
               
     
    }

    @Override
    public void start(Stage primaryStage)  {
        Scene s= new Scene(new Interfaz().getRoot());
        primaryStage.setTitle("Creacion de clientes");
        primaryStage.setScene(s);
        primaryStage.show();
        primaryStage.centerOnScreen();
    }
}