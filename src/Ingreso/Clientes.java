/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ingreso;

import Grafica.Interfaz;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author liz
 * esta clase es para serializar los datos que se ingresan por pantalla
 */

public class Clientes implements Serializable{
    
    private int cedulaC;
    private String nombreC;
    private String direccionC;
    private String correoC;

    public Clientes(int cedulaC, String nombreC, String direccionC, String correoC) {
        this.cedulaC = cedulaC;
        this.nombreC = nombreC;
        this.direccionC = direccionC;
        this.correoC = correoC;
    }
    
     
    
    public int getCedulaC() {
        return cedulaC;
    }

    public String getNombreC() {
        return nombreC;
    }

    public String getDireccionC() {
        return direccionC;
    }

    public String getCorreoC() {
        return correoC;
    }

    @Override
    public String toString() {
        return nombreC ;
    }
    
    
    
    
    
    
}
