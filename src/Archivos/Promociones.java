/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

/**
 *
 * @author CltControl
 */
public class Promociones {
    private int idHotel;
    private String descripcion;

    public Promociones(int idHotel, String descripcion) {
        this.idHotel = idHotel;
        this.descripcion = descripcion;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    
}
