/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author liz
 */
public class HotelServicio {
    
    private final SimpleStringProperty nombreHotel;
    private int idHotel;
    private int idServicio;
    private final SimpleStringProperty servicio;

    public HotelServicio(String nombreHotel, int idHotel, String servicio) {
        this.nombreHotel = new SimpleStringProperty(nombreHotel);
        this.idHotel = idHotel;
        //this.idServicio = idServicio;
        this.servicio = new SimpleStringProperty(servicio);
    }

    public String getNombreHotel() {
        return nombreHotel.get();
    }

    public int getIdHotel() {
        return idHotel;
    }

   

    public String getServicio() {
        return servicio.get();
    }

    @Override
    public String toString() {
        return "HotelServicio{" + "nombreHotel=" + nombreHotel + ", idHotel=" + idHotel + ", servicio=" + servicio + '}';
    }
    
    
    
}
