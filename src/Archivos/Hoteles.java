/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author liz
 */
public class Hoteles {
    private int idHotel;
    private int idCiudad;
    private final String nombreHotel;
    private String descripcionHotel;
    private String tarjetas;
    private String ubicacion;
    private String  direccion;
    private String web;
    private String clasificacion;
    private String foto;
    private float latitud;
    private float longitud;

    public Hoteles(int idHotel, int idCiudad, String nombreHotel, String descripcionHotel, String tarjetas, String ubicacion, String direccion, String web, String clasificacion, String foto, float latitud, float longitud) {
        this.idHotel = idHotel;
        this.idCiudad = idCiudad;
        this.nombreHotel = nombreHotel;
        this.descripcionHotel = descripcionHotel;
        this.tarjetas = tarjetas;
        this.ubicacion = ubicacion;
        this.direccion = direccion;
        this.web = web;
        this.clasificacion = clasificacion;
        this.foto = foto;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public String getNombreHotel() {
        return nombreHotel;
    }

    public String getDescripcionHotel() {
        return descripcionHotel;
    }

    public String getTarjetas() {
        return tarjetas;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getWeb() {
        return web;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public String getFoto() {
        return foto;
    }

    public float getLatitud() {
        return latitud;
    }

    public float getLongitud() {
        return longitud;
    }
    
}
