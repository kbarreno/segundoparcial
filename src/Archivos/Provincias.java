/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

/**
 *
 * @author liz
 */
public class Provincias {
    private int idProvincia;
    private String provincia;
    private String descripcion;
    private String web;

    public Provincias(int idProvincia, String provincia, String descripcion, String web) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.descripcion = descripcion;
        this.web = web;
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getWeb() {
        return web;
    }

    @Override
    public String toString() {
        return provincia ;
    }
    
}
