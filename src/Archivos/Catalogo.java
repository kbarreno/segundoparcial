/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

/**
 *
 * @author liz
 */
public class Catalogo {
    private int idCat;
    private String servicio;

    public Catalogo(int idCat, String servicio) {
        this.idCat = idCat;
        this.servicio = servicio;
    }

    public int getIdCat() {
        return idCat;
    }

    public String getServicio() {
        return servicio;
    }
    
    
}
