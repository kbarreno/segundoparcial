package Archivos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author liz
 */
public class Lectura {
    private static ArrayList<Catalogo> atalogos;
    
    private static ArrayList<Ciudades> iudades;
    private static ArrayList<Habitaciones> abitaciones;
    private static ArrayList<Hoteles> oteles;
    private static ArrayList<Provincias> rovincias;
    private static ArrayList<Servicios> ervicios;
    private static ArrayList<HotelServicio> HoSe = new ArrayList<>();
    private static ArrayList<Promociones> promociones;
    
    
/**
 * se crea una lista con el nombre de todoslos csv para asi poder leerlos
 * se leen todos los csv y se almacenan los datos en listas con su tipo de clase
 * 
 * @throws IOException 
 */
    public static void readFIles() throws IOException {
        String[] arch={"catalogo.csv","ciudades.csv","habitaciones.csv","hoteles1.csv","provincias.csv","servicios.csv","promociones.csv"};
        atalogos = new ArrayList<>();
        promociones = new ArrayList<>();
        iudades = new ArrayList<>();
        abitaciones = new ArrayList<>();
        oteles = new ArrayList<>();
        rovincias = new ArrayList<>();
        ervicios = new ArrayList<>();
        
        for(int i=0; i < arch.length ;i++){
            try(BufferedReader b = new BufferedReader(new FileReader(arch[i]))){
                String linea;
                b.readLine();
                
               while((linea= b.readLine())!= null){
                    try{ 
                    String[]c = linea.split("\\|");
                    
                    
               switch (i+1) 
                {
                case 1:  //catalogo
                    
                    Catalogo ct = new Catalogo(Integer.parseInt(c[0]),c[1]);
                    atalogos.add(ct);    
                    
                    break;
                    
                case 2:  //ciudades
                    Ciudades u = new Ciudades(Integer.parseInt(c[0]),Integer.parseInt(c[1]),c[2]);
                    iudades.add(u); 
                 
                    break;
                    
                case 3:  //habitaciones
                    Habitaciones h = new Habitaciones(Integer.parseInt(c[0]),Integer.parseInt(c[1]),c[2],Float.parseFloat(c[3]),Float.parseFloat(c[4]),Float.parseFloat(c[5]));
                    abitaciones.add(h); 
                 
                    break;
                    
                case 4: //hoteles
                    float latitud=0;
                    float longitud=0;
                    
                    if (!c[10].equals("-")){
                        
                        latitud = Float.parseFloat(c[10].trim());
                    }
                    if (!c[11].equals("-")){
                       
                        longitud = Float.parseFloat(c[11].trim());
                    }
                    
                    Hoteles o = new Hoteles(Integer.parseInt(c[0]),Integer.parseInt(c[1]),c[2],c[3],c[4],c[5],c[6],c[7],c[8],c[9],latitud,longitud);
                    oteles.add(o); 
                    
                    break;
                    
                case 5:  //provincias  
                    Provincias p = new Provincias(Integer.parseInt(c[0]),c[1],c[2],c[3]);
                    rovincias.add(p);
                 
                    break;
                    
                    
                    
                case 6: //servicios
                    Servicios s = new Servicios(Integer.parseInt(c[0]),Integer.parseInt(c[1]),Integer.parseInt(c[2]),c[3]);
                    ervicios.add(s);
                 
                    break;
                
                case 7: //servicios
                    Promociones pro = new Promociones(Integer.parseInt(c[0]),c[1]);
                    promociones.add(pro);
                 
                    break;    
                default: 
                    
                     break;

               }
               } catch (ArrayIndexOutOfBoundsException e){
                   System.out.println("Error en la linea: "+linea+ " "+e);
            }
            }
            }catch (FileNotFoundException ex){
                System.out.println("archico canciones.txt no existe"+ex);
            }catch (IOException ex) {
                System.out.println("otros erroes de IO "+ex.getMessage());;
            }
         
        }
        llenarHotelServicio();   
    }

    public static ArrayList<Promociones> getPromociones() {
        return promociones;
    }

    /**
     * se creo una clase tipo hotel/servicio para luego ser utilizado en la busqueda de hoteles por hotel o servicio
     */
  
     
    public static void llenarHotelServicio(){
        
       for(int i=1; i <220 ;i++){
           //System.out.println("df");
            ArrayList<String> servicios=new ArrayList();
            
            for (Servicios s: ervicios){
                //System.out.println("prueba 1");
                if(s.getIdHotel()==i){
                    int num = s.getIdServicio();
                    for (Catalogo c: atalogos){
                        if(num==c.getIdCat()){
                            String ser = c.getServicio();
                            servicios.add(ser);
                            System.out.println(ser);
                        }
                    }
                    
                }
            }
            String l = String.join(",", servicios);
            for (Hoteles h: oteles){
                int n = h.getIdHotel();
                if(n==i){
                    HotelServicio ct = new HotelServicio(h.getNombreHotel(),i,l);
                    HoSe.add(ct);
                }
            }
            
            
        }
    }

    public static ArrayList<Catalogo> getAtalogos() {
        return atalogos;
    }

    public static ArrayList<Ciudades> getIudades() {
        return iudades;
    }

    public static ArrayList<Habitaciones> getAbitaciones() {
        return abitaciones;
    }

    public static ArrayList<Hoteles> getOteles() {
        return oteles;
    }

    public static ArrayList<Provincias> getRovincias() {
        return rovincias;
    }
    public static ArrayList<HotelServicio> getHoSe() {
        return HoSe;
    }

    public static ArrayList<Servicios> getErvicios() {
        return ervicios;
    }

    
}


    
