/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

/**
 *
 * @author liz
 */
public class Habitaciones {
    private int idHabitaciones;
    private int idHotel;
    private String tipoHabitacion;
    private float tarifaSencilla;
    private float tarifaDoble;
    private float tarifaTriple;

    public Habitaciones(int idHabitaciones, int idHotel, String tipoHabitacion, float tarifaSencilla, float tarifaDoble, float tarifaTriple) {
        this.idHabitaciones = idHabitaciones;
        this.idHotel = idHotel;
        this.tipoHabitacion = tipoHabitacion;
        this.tarifaSencilla = tarifaSencilla;
        this.tarifaDoble = tarifaDoble;
        this.tarifaTriple = tarifaTriple;
    }
    public Habitaciones(String tipoHabitacion){
        this.tipoHabitacion= tipoHabitacion;
        
    }

    public int getIdHabitaciones() {
        return idHabitaciones;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    public double getTarifaSencilla() {
        return tarifaSencilla;
    }

    public double getTarifaDoble() {
        return tarifaDoble;
    }

    public double getTarifaTriple() {
        return tarifaTriple;
    }

    @Override
    public String toString() {
        return  tipoHabitacion;
    }
    
    
}
