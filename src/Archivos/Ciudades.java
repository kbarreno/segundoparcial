/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

/**
 *
 * @author liz
 */
public class Ciudades {
    private int idCiudad;
    private int idProvincia;
    private String Ciudad;

    public Ciudades(int idCiudad, int idProvincia, String Ciudad) {
        this.idCiudad = idCiudad;
        this.idProvincia = idProvincia;
        this.Ciudad = Ciudad;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public String getCiudad() {
        return Ciudad;
    }

    @Override
    public String toString() {
        return Ciudad ;
    }
    
    
}