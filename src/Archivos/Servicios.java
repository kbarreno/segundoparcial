/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

/**
 *
 * @author liz
 */
public class Servicios {
    private int secuencia;
    private int idHotel;
    private int idServicio;
    private String estado;

    public Servicios(int secuencia, int idHotel, int idServicio, String estado) {
        this.secuencia = secuencia;
        this.idHotel = idHotel;
        this.idServicio = idServicio;
        this.estado = estado;
    }

    public int getSecuencia() {
        return secuencia;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public String getEstado() {
        return estado;
    }
    
    
}
